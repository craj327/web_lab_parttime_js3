"use strict";

// TODO Car

var car = {
    year: 2007,
    make: "BMW",
    model: "323i",
    bodyType: "Sedan",
    transmission: "Triptonic",
    odometer: 68512,
    price: "$16000",
}
// TODO Music

var musicAlbum = {
    title: 1989,
    artist: "Taylor Swift",
    year: 2014,
    genre: "Pop",
    tracks: ["Welcome to New York", 
             "Blank Space",
             "Style",
             "Out of the Woods",
             "All You Had to Do Was Stay",
             "Shake It Off",
             "I Wish You Would",
             "Bad Blood",
             "Wildest Dreams",
             "How You Get the Girl",
             "This Love",
             "I Know Places",
             "Clean"],
    toString: function()
    {
        var album = 'Album: ' + '"' + this.title + '"' + ' , released in ' + this.year + ' by ' + this.artist;
        return album;
    }
}

var album = musicAlbum.toString();
console.log(album);