"use strict";

// TODO Create object prototypes here

function musicAlbum (title, artist, year, genre, tracks) 
{
    this.title = title; 
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = tracks

    this.getAlbum = function() {
        var str = "Album: " + '"' + this.title + '"' + ', released in ' + this.year + ' by ' + this.artist + ". Track listing:" + "\n";

        for(var i=0; i < this.tracks.length; i ++) {
            str = str + this.tracks[i] + "\n"
        }
        
        return str;
    }
}
 
// TODO Create functions here

function getAlbums()
{
    var albums = [];

    var taylorSwift1 = new musicAlbum (1989, "Taylor Swift", 2014, "Pop",["Welcome to New York","Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", "I Wish You Would", "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"])

    var taylorSwift2 = new musicAlbum ("Reputation", "Taylor Swift", 2017, "Pop", ["...Ready for It?", "End Game", "I Did Something Bad", "Don't Blame Me", "Delicate", "Look What You Made Me Do", "So It Goes...", "Gorgeous", "Getaway Car", "King of My Heart", "Dancing with Our Hands Tied", "Dress", 	"This Is Why We Can't Have Nice Things", "Call It What You Want", "New Year's Day"])

    albums[0] = taylorSwift1;
    albums[1] = taylorSwift2;

    return albums;
}

// TODO Complete the program here

var albums = getAlbums();

for (var i = 0; i < albums.length; i++){
    console.log(albums[i].getAlbum());
}