"use strict";

var text = "This is some text.";
text = text.toUpperCase();

var frequencies = [];

for (var i = 0; i < text.length; i++) {

    var character = text.charAt(i);

    if(frequencies[character] == undefined)
    {
        frequencies[character] = 1;
    }
    else
    {
        frequencies[character] = frequencies[character] + 1;
    }
}

// TODO Print out the result
console.log("Text: " + text);

for(var i in frequencies)
{
     console.log(i + " appears " + frequencies[i] + ' times in the text');
}